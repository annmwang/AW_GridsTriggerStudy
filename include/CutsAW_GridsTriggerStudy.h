#ifndef CutsAW_GridsTriggerStudy_H
#define CutsAW_GridsTriggerStudy_H

#include "EventBase.h"

class CutsAW_GridsTriggerStudy {

public:
    CutsAW_GridsTriggerStudy(EventBase* eventBase);
    ~CutsAW_GridsTriggerStudy();
    bool AW_GridsTriggerStudyCutsOK();

private:
    EventBase* m_event;
};

#endif
