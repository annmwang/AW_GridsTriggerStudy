#ifndef AW_GridsTriggerStudy_H
#define AW_GridsTriggerStudy_H

#include "Analysis.h"

#include "CutsAW_GridsTriggerStudy.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class AW_GridsTriggerStudy : public Analysis {

public:
    AW_GridsTriggerStudy();
    ~AW_GridsTriggerStudy();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsAW_GridsTriggerStudy* m_cutsAW_GridsTriggerStudy;
    ConfigSvc* m_conf;
};

#endif
