#include "AW_GridsTriggerStudy.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsAW_GridsTriggerStudy.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Choose 0 for November runs, 1 for Run 6621, 2 for Run 6622
int run_group = 2;

// Constructor
AW_GridsTriggerStudy::AW_GridsTriggerStudy()
    : Analysis()
{    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("eventTPC");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("AW_GridsTriggerStudy Analysis");

    // Setup the analysis specific cuts.
    m_cutsAW_GridsTriggerStudy = new CutsAW_GridsTriggerStudy(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
AW_GridsTriggerStudy::~AW_GridsTriggerStudy()
{
    delete m_cutsAW_GridsTriggerStudy;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void AW_GridsTriggerStudy::Initialize()
{
    INFO("Initializing AW_GridsTriggerStudy Analysis");
}

// Execute() - Called once per event.
void AW_GridsTriggerStudy::Execute()
{
  // Event info
  // Event info
  double firstTriggerTime;
  if (run_group == 0)
    firstTriggerTime = 1637989583.;
  else if (run_group == 1)
    firstTriggerTime = 1639453528.;
  else if (run_group == 2)
    firstTriggerTime = 1639474372.;

  //ER-only 1634511521
  double triggerEndTime;
  if (run_group == 0)
    triggerEndTime = 140040.;
  else if (run_group == 1)
    triggerEndTime = 20700.;
  else if (run_group == 2)
    triggerEndTime = 20400.;
  //ER-only 50400.
  double triggerTime      = (*m_event->m_eventHeader)->triggerTimeStamp_s+(*m_event->m_eventHeader)->triggerTimeStamp_ns*pow(10,-9)-firstTriggerTime;

  int nbins              = triggerEndTime / 60.;
  int ihour              = floor(triggerTime/60./60.);
  int triggerType        = (*m_event->m_eventHeader)->triggerType;
  int runID              = (*m_event->m_eventHeader)->runID;
  int eventID              = (*m_event->m_eventHeader)->eventID;

  m_hists->BookFillHist("evt_cutflow", 10, -0.5, 9.5, 0);
  
  // S2-only trigger
  if (triggerType == 128) {
    m_hists->BookFillHist("evt_cutflow", 10, -0.5, 9.5, 1);
    m_hists->BookFillHist("triggerTime_s2",nbins, 0., triggerEndTime, float(triggerTime));
    // Loop through all pulses, categorize pulses
    int nPulses = (*m_event->m_tpcPulses)->nPulses;
    int min_dt = 100000;
    int neo = -1;
    //int target_trig = 0;
    int target_trig = -2000;
    for (int i = 0; i < nPulses; i++){
      // look for pulse that's closest in time to 0
      int start_time = (*m_event->m_tpcPulses)->pulseStartTime_ns[i];
      int end_time   = (*m_event->m_tpcPulses)->pulseEndTime_ns[i];
      // harmonize with drew
      if ((abs(start_time-target_trig) < min_dt) && ((*m_event->m_tpcPulses)->pulseArea_phd[i] > 5)) {
        min_dt = abs(start_time-target_trig);
        neo = i;
      }
    }
    if (neo >= 0){
      m_hists->BookFillHist("evt_cutflow", 10, -0.5, 9.5, 2);
      string classification = (*m_event->m_tpcPulses)->classification[neo] ;
      int AFT_5ns     = (*m_event->m_tpcPulses)->areaFractionTime5_ns[neo];
      int AFT_95ns    = (*m_event->m_tpcPulses)->areaFractionTime95_ns[neo];
      float pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[neo];
      float s2Xposition_cm  = (*m_event->m_tpcPulses)->s2Xposition_cm[neo];
      float s2Yposition_cm  = (*m_event->m_tpcPulses)->s2Yposition_cm[neo];
      float topCentroidXposition_cm  = (*m_event->m_tpcPulses)->topCentroidX_cm[neo];
      float topCentroidYposition_cm  = (*m_event->m_tpcPulses)->topCentroidY_cm[neo];
      if ( (classification == "S2") || (classification == "SE")){
        m_hists->BookFillHist("SES2_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("SES2_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_SES2",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("SES2_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));

        // SR-1
        // Emitter 0 small circle
        
        double ctr0_x =  -65; 
        double ctr0_y = 1; 
        if (run_group == 0){
          ctr0_x = -65;
          ctr0_y = 9;
        }
        if (sqrt(pow(s2Xposition_cm-ctr0_x,2)+pow(s2Yposition_cm-ctr0_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter0_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter0_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter0_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter0",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }
        
        // Emitter 1
        double ctr1_x = 68; 
        double ctr1_y = -2; 
        if (run_group == 0){
          ctr1_x = -34;
          ctr1_y = 44.5;
        }
        if (sqrt(pow(s2Xposition_cm-ctr1_x,2)+pow(s2Yposition_cm-ctr1_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter1_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter1_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter1_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter1",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }

        // Emitter 2
        double ctr2_x = 14; 
        double ctr2_y = -62; 
        if (sqrt(pow(s2Xposition_cm-ctr2_x,2)+pow(s2Yposition_cm-ctr2_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_emitter2_circle",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_emitter2_circle",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_emitter2_circle",2000,0.,1000.,pulseArea);
          m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_SES2_emitter2",500,560.,10000,2000,0,1000, AFT_95ns-AFT_5ns, pulseArea);
        }

        // Background
        double bkg0_x = -20; 
        double bkg0_y = -61.85; 
        if (run_group == 0){
          bkg0_x = 65;
          bkg0_y = 9;
        }
        if (sqrt(pow(s2Xposition_cm-bkg0_x,2)+pow(s2Yposition_cm-bkg0_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg0",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg0",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg0",2000,0.,1000.,pulseArea);
        }
        double bkg1_x = 2; 
        double bkg1_y = 68; 
        if (run_group == 0){
          bkg1_x = 34;
          bkg1_y = 44.5;
        }
        if (sqrt(pow(s2Xposition_cm-bkg1_x,2)+pow(s2Yposition_cm-bkg1_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg1",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg1",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg1",2000,0.,1000.,pulseArea);
        }

        double bkg2_x = 49.4; //34;
        double bkg2_y = -40; //44.5;
        if (sqrt(pow(s2Xposition_cm-bkg2_x,2)+pow(s2Yposition_cm-bkg2_y,2)) < 10){
          m_hists->BookFillHist("SES2_Y_vs_X_bkg2",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("SES2_triggerTime_bkg2",nbins, 0., triggerEndTime, float(triggerTime));
          m_hists->BookFillHist("pulseArea_SES2_bkg2",2000,0.,1000.,pulseArea);
        }
      }
      if (classification == "SE"){

        m_hists->BookFillHist("SE_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("SE_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_SE",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("SE_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
      }
      else if (classification == "SPE"){
      m_hists->BookFillHist("SPE_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
      m_hists->BookFillHist("SPE_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
      m_hists->BookFillHist("pulseArea_SPE",500,0.,5000.,pulseArea);
      m_hists->BookFillHist("SPE_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
      }
      else if (classification == "S1"){
        m_hists->BookFillHist("S1_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("S1_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_S1",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("S1_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
        m_hists->BookFillHist("pulseArea_tpc_vs_AFT95mAFT5_S1trigpulse",100,0.,1000,500,0,5000, AFT_95ns-AFT_5ns, pulseArea);
      }
      else if (classification == "S2"){
         m_hists->BookFillHist("S2_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("S2_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_S2",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("S2_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
        if (pulseArea > 400){
          m_hists->BookFillHist("S2_Y_vs_X_pulseArea_g400",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
          m_hists->BookFillHist("S2_Y_vs_X_topCentroid_g400",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
          m_hists->BookFillHist("S2_triggerTime_pulseArea_g400",nbins, 0., triggerEndTime, float(triggerTime));
        }
      }
      else if (classification == "MPE"){
        m_hists->BookFillHist("MPE_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("MPE_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_MPE",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("MPE_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
      }
      else if (classification == "Other"){
        m_hists->BookFillHist("Other_Y_vs_X",500,-100,100,500,-100,100, s2Xposition_cm, s2Yposition_cm);
        m_hists->BookFillHist("Other_Y_vs_X_topCentroid",500,-100,100,500,-100,100, topCentroidXposition_cm, topCentroidYposition_cm);
        m_hists->BookFillHist("pulseArea_Other",500,0.,5000.,pulseArea);
        m_hists->BookFillHist("Other_triggerTime",nbins, 0., triggerEndTime, float(triggerTime));
      }
      else {
        std::cout << "WHAT: " << classification << std::endl;
      }
    }
  }
}

// Finalize() - Called once after event loop.
void AW_GridsTriggerStudy::Finalize()
{
    INFO("Finalizing AW_GridsTriggerStudy Analysis");
}
