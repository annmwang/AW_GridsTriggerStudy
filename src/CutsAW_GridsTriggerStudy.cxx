#include "CutsAW_GridsTriggerStudy.h"
#include "ConfigSvc.h"

CutsAW_GridsTriggerStudy::CutsAW_GridsTriggerStudy(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsAW_GridsTriggerStudy::~CutsAW_GridsTriggerStudy()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsAW_GridsTriggerStudy::AW_GridsTriggerStudyCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
